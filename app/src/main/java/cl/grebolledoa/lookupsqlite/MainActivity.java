package cl.grebolledoa.lookupsqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import cl.grebolledoa.lookupsqlite.adapter.CarsAdapter;
import cl.grebolledoa.lookupsqlite.helpers.CarsSQLiteHelper;
import cl.grebolledoa.lookupsqlite.models.Car;

public class MainActivity extends AppCompatActivity {

    private SQLiteDatabase db;

    private CarsSQLiteHelper carsHelper;

    private List<Car> cars;

    private ListView listView;

    private CarsAdapter carsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        carsHelper = new CarsSQLiteHelper(this, "DBTest", null, 1);

        //escritura
        db = carsHelper.getWritableDatabase();

        //lectura
        //db = carsHelper.getReadableDatabase();

        listView = findViewById(R.id.listview);

        cars = new ArrayList<>();

        carsAdapter = new CarsAdapter(this,cars, R.layout.itemcar);

        listView.setAdapter(carsAdapter);

        create();
        getAllCars();
    }

    private void create(){
        if(db!=null){
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("name", "Evoque");
            nuevoRegistro.put("color", "Red");

            db.insert("Cars", null, nuevoRegistro);
        }
    }

    private void getAllCars(){
        Cursor cursor = db.rawQuery("select * from Cars", null);


        if(cursor.moveToFirst()){
            while (!cursor.isAfterLast()){
                int vin = cursor.getInt(cursor.getColumnIndex("VIN"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String color = cursor.getString(cursor.getColumnIndex("color"));
                cars.add(new Car(vin, name, color));
                cursor.moveToNext();
            }
        }

        for(Car item : cars){
            Log.e("vin", String.valueOf(item.getVIN()));
            Log.e("name", item.getName());
            Log.e("color", item.getColor());
        }


    }

    @Override
    protected void onDestroy() {
        db.close();
        super.onDestroy();
    }
}