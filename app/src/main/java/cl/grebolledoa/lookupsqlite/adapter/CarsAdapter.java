package cl.grebolledoa.lookupsqlite.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cl.grebolledoa.lookupsqlite.R;
import cl.grebolledoa.lookupsqlite.models.Car;

public class CarsAdapter extends BaseAdapter {

    private Context context;
    private List<Car> list;
    private int layout;

    public CarsAdapter(Context context, List<Car> list, int layout) {
        this.context = context;
        this.list = list;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(layout, null);
        TextView vin = convertView.findViewById(R.id.textViewVIN);
        TextView name = convertView.findViewById(R.id.textViewName);
        TextView color = convertView.findViewById(R.id.textViewColor);

        Car currentCar = list.get(position);

        vin.setText(String.valueOf(currentCar.getVIN()));
        name.setText(String.valueOf(currentCar.getName()));
        color.setText(currentCar.getColor());
        return convertView;
    }
}
